# API Spec

## Create Product

Request :
-Method : POST
- Endpoint : `/api/products`
- Header :
    - Content-Type: application/json
    - Accept: Application/json
- Body :

```json
{ 
  "id" : "String, unique",
  "name" : "String",
  "price" : "Long",
  "qty" : "Integer"
}
```

Response :
```json
{ 
  "code" : "number",
  "status" : "string",
  "data" : {
      "id" : "String, unique",
      "name" : "String",
      "price" : "Long",
      "qty" : "Integer",
      "createdAt" : "date",
      "updateAt" : "null"
  }
}
```

## Get Product

Request :
-Method : GET
- Endpoint : `/api/products/{id_product}`
- Header :    
    - Accept: Application/json

Response :
```json
{
  "code" : "number",
  "status" : "string",
  "data" : {
      "id" : "String, unique",
      "name" : "String",
      "price" : "Long",
      "qty" : "Integer",
      "createdAt" : "date"
  }
}
```

## Update Product

Request :
-Method : PUT
- Endpoint : `/api/products/{id_product}`
- Header :
    - Content-Type: application/json
    - Accept: Application/json
- Body :    
```json
{
  "name" : "String",
  "price" : "Long",
  "qty" : "Integer"
}
```

Response :
```json
{
  "code" : "number",
  "status" : "string",
  "data" : {
      "id" : "String, unique",
      "name" : "String",
      "price" : "Long",
      "qty" : "Integer",
      "createdAt" : "date",
      "updateAt" : "date"
  }
}
```

## List Product

Request :
-Method : GET
- Endpoint : `/api/products/`
- Header :
    - Accept: Application/json
- Query param :
    - size : number,
    - page : number,

Response :
```json
{
  "code" : "number",
  "status" : "string",
  "data" : [
    {
        "id" : "String, unique",
        "name" : "String",
        "price" : "Long",
        "qty" : "Integer",
        "createdAt" : "date",
        "updateAt" : "date"
    },
    {
        "id" : "String, unique",
        "name" : "String",
        "price" : "Long",
        "qty" : "Integer",
        "createdAt" : "date",
        "updateAt" : "date"
    }
  ]
}
```

## Delete Product

Request :
-Method : DELETE
- Endpoint : `/api/products/{id_product}`
- Header :    
    - Accept: Application/json
    
Response :
```json
{
  "code" : "number",
  "status" : "string"
}
```