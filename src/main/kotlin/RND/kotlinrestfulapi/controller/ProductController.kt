package RND.kotlinrestfulapi.controller

import RND.kotlinrestfulapi.model.request.ListProductRequest
import RND.kotlinrestfulapi.model.request.CreateProductRequest
import RND.kotlinrestfulapi.model.request.UpdateProductRequest
import RND.kotlinrestfulapi.model.response.ProductResponse
import RND.kotlinrestfulapi.model.response.WebResponse
import RND.kotlinrestfulapi.service.ProductService
import org.springframework.web.bind.annotation.*

@RestController
class ProductController(
    val productService: ProductService
) {

    @PostMapping(
        value = ["/api/products"],
        produces = ["application/json"],
        consumes = ["application/json"]
    )
    fun createProduct(@RequestBody
                      body: CreateProductRequest
    ): WebResponse<ProductResponse>{
        val response =  productService.create(body)
        return WebResponse(
            code = 200,
            status = "OK",
            data = response
        )
    }

    @GetMapping(
        value = ["/api/products/{idProduct}"],
        produces = ["application/json"]
    )
    fun getProduct(@PathVariable("idProduct")
                   id: String
    ):WebResponse<ProductResponse>{
        val response = productService.get(id)
        return WebResponse(
            code = 200,
            status = "OK",
            data = response
        )
    }

    @PutMapping(
        value = ["/api/products"],
        produces = ["application/json"],
        consumes = ["application/json"]
    )
    fun updateProduct(@PathVariable("idProduct")
                      id: String,

                      @RequestBody
                      body: UpdateProductRequest,
    ): WebResponse<ProductResponse>{
        val response = productService.update(id, body)

        return WebResponse(
            code = 200,
            status = "OK",
            data = response
        )
    }

    @DeleteMapping(
        value = ["api/products/{idProduct}"],
        produces = ["application/json"]
    )
    fun deleteProduct(@PathVariable("idProduct")
                      id: String
    ): WebResponse<String> {
        productService.delete(id)
        return WebResponse(
            code = 200,
            status = "OK",
            data = id
        )
    }

    @GetMapping(
        value = ["/api/products"],
        produces = ["application/json"]
    )
    fun listProducts(@RequestParam(value = "size", defaultValue = "10")
                     size: Int,

                     @RequestParam(value = "page", defaultValue = "0")
                     page: Int,
    ) : WebResponse<List<ProductResponse>>{
        val request = ListProductRequest(page, size)
        val response = productService.list(request)

        return WebResponse(
            code = 200,
            status = "OK",
            data = response
        )

    }
}