package RND.kotlinrestfulapi.service

import RND.kotlinrestfulapi.model.request.CreateProductRequest
import RND.kotlinrestfulapi.model.request.ListProductRequest
import RND.kotlinrestfulapi.model.request.UpdateProductRequest
import RND.kotlinrestfulapi.model.response.ProductResponse

interface ProductService {

    fun create(createProductRequest: CreateProductRequest): ProductResponse

    fun get(id: String): ProductResponse

    fun update(id: String, updateProductRequest: UpdateProductRequest): ProductResponse

    fun delete(id: String)

    fun list(listProductRequest: ListProductRequest): List<ProductResponse>

}