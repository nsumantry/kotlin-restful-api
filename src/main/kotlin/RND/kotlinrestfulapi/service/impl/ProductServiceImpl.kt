package RND.kotlinrestfulapi.service.impl

import RND.kotlinrestfulapi.entity.Product.Product
import RND.kotlinrestfulapi.error.NotFoundException
import RND.kotlinrestfulapi.model.request.CreateProductRequest
import RND.kotlinrestfulapi.model.request.ListProductRequest
import RND.kotlinrestfulapi.model.request.UpdateProductRequest
import RND.kotlinrestfulapi.model.response.ProductResponse
import RND.kotlinrestfulapi.repository.ProductRepository
import RND.kotlinrestfulapi.service.ProductService
import RND.kotlinrestfulapi.validation.ValidationUtil
import org.springframework.data.domain.PageRequest
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.util.*
import java.util.stream.Collectors

@Service
class ProductServiceImpl (
    val repository: ProductRepository,
    val validationUtil: ValidationUtil
) : ProductService {

    override fun create(request: CreateProductRequest): ProductResponse {
        validationUtil.validate(request)

        val product = Product(
            id = request.id!!,
            name = request.name!!,
            price = request.price!!,
            qty = request.qty!!,
            createdAt = Date(),
            updateAt = null
        )

        repository.save(product)

        return  return convertProductToProductResponse(product)
    }

    override fun get(id: String): ProductResponse {
        val product = findProductByIdOrThrowNotFound(id)
        return convertProductToProductResponse(product)
    }

    override fun update(id: String, request: UpdateProductRequest): ProductResponse {
        val product = findProductByIdOrThrowNotFound(id)

        validationUtil.validate(request)

        product.apply {
            name = request.name!!
            price = request.price!!
            qty = request.qty!!
            updateAt = Date()
        }

        repository.save(product)

        return convertProductToProductResponse(product)
    }

    override fun delete(id: String) {
        val product = findProductByIdOrThrowNotFound(id)
        repository.delete(product)
    }

    override fun list(request: ListProductRequest): List<ProductResponse> {
        val page = repository.findAll(PageRequest.of(request.page, request.size))
        val products: List<Product> = page.get().collect(Collectors.toList())
        return products.map { convertProductToProductResponse(it) }
    }


    private fun findProductByIdOrThrowNotFound(id: String): Product {
        val product = repository.findByIdOrNull(id)
        if (product == null){
            throw NotFoundException()
        }else{
            return product
        }
    }

    private fun convertProductToProductResponse(product: Product) : ProductResponse{
        return ProductResponse(
            id = product.id,
            name = product.name,
            price = product.price,
            qty = product.qty,
            createdAt = product.createdAt,
            updateAt = product.updateAt
        )
    }
}