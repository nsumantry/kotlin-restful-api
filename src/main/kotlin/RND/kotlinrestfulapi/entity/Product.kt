package RND.kotlinrestfulapi.entity.Product

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name= "products")
class Product (

    @Id
    val id: String,

    @Column(name="name")
    var name: String,

    @Column(name="price")
    var price: Long,

    @Column(name="qty")
    var qty: Int,

    @Column(name="created_at")
    var createdAt: Date,

    @Column(name="update_at")
    var updateAt: Date?

)