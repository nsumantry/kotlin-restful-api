package RND.kotlinrestfulapi.model.request

data class ListProductRequest(

    val page: Int,

    val size: Int
)