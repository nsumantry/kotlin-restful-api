package RND.kotlinrestfulapi.model.response

import java.util.*
import javax.persistence.Column
import javax.persistence.Id

data class ProductResponse (

    val id: String,

    val name: String,

    val price: Long,

    val qty: Int,

    val createdAt: Date,

    val updateAt: Date?

)