package RND.kotlinrestfulapi.repository

import RND.kotlinrestfulapi.entity.Product.Product
import org.springframework.data.jpa.repository.JpaRepository

interface ProductRepository : JpaRepository<Product, String>{

}